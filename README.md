Image size

| REPOSITORY | TAG | SIZE |
| ------ | ------ | ------ |
| sixdeex/bench1  | alpine-jre8 | 119MB |
| sixdeex/bench1  | graalvm | 1.82GB |
| sixdeex/bench2 | alpine-jre8 | 144MB |
| sixdeex/bench2 | graalvm | 1.81GB |

Build env: graalvm-ce-19.2.1
